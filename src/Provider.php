<?php

namespace Eksik\Package;

use Eksik\Package\Classes\ClassA;
use Eksik\Package\Contracts\ResponseInterface;
use Eksik\Package\Services\ExampleService;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

/**
 * Class Provider
 * @package Eksik\Package
 */
class Provider extends ServiceProvider
{

    public function boot()
    {
        $this->mapBinding();
    }

    private function mapBinding()
    {
        $this->app->bind(ResponseInterface::class, function (Application $app) {
            return $app->make(ClassA::class);
        });
        $this->app->bind('eksik', function (Application $app) {
            return $app->make(ExampleService::class);
        });
    }

    public function register()
    {
        $this->registerRoutes();
    }

    private function registerRoutes()
    {
        Route::group([
            'prefix' => 'eksik',
            'namespace' => 'Eksik\\Package\\Http\\Controllers',
            'middleware' => 'web'
        ], function () {
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        });
    }
}
