<?php

namespace Eksik\Package\Contracts;

interface ResponseInterface
{
    public function test();

    public function testModel();
}