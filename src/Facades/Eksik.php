<?php

namespace Eksik\Package\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Eksik
 * @package Eksik\Package\Facades
 * @method static test()
 * @method static testModel()
 */
class Eksik extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'eksik';
    }
}
