<?php

namespace Eksik\Package\Services;

use Eksik\Package\Contracts\ResponseInterface;

class ExampleService
{
    protected $model;

    public function __construct(ResponseInterface $response)
    {
        $this->model = $response;
    }

    public function test()
    {
        return $this->model->test();
    }

    public function testModel()
    {
        return $this->model->testModel();
    }
}