<?php

namespace Eksik\Package\Http\Controllers;

use Eksik\Package\Facades\Eksik;

class HelloController extends BaseController
{
    public function index()
    {
        $value = Eksik::test();
        $value1 = Eksik::testModel();

        dd($value, $value1);
    }
}
