<?php

namespace Eksik\Package\Classes;

use Eksik\Package\Contracts\ResponseInterface;

class ClassA implements ResponseInterface
{
    public function test()
    {
        return __CLASS__ . ': cos';
    }

    public function testModel()
    {
        return '';
    }
}